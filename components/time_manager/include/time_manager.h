#ifndef __TIME_MANAGER__H__
#define __TIME_MANAGER__H__

#ifdef __cplusplus
extern "C" {
#endif

#include "esp_event.h"  // Inclui biblioteca de funções de eventos
#include "esp_log.h"    // Inclui biblioteca de funções de log
#include "esp_system.h" // Inclui biblioteca de funções de sistema
#include <esp_sntp.h>   // Inclui biblioteca de funções de sistema de tempo
#include <string.h>
#include <sys/time.h> // Inclui biblioteca de funções de tempo
#include <time.h>     // Inclui biblioteca de funções de tempo

esp_err_t init_time_manager(void);

esp_err_t sync_time(void);

esp_err_t get_time(time_t *time_p);

esp_err_t get_time_str(char *time_str);

#ifdef __cplusplus
}
#endif

#endif //!__TIME_MANAGER__H__