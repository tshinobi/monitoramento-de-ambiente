/**
 * @file storage_manager.c
 * @author Julio Nunes Avelar (julio@bzoide.dev)
 * @brief
 * @version 0.1
 * @date 2023-09-23
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "storage_manager.h"

#include <stdio.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/unistd.h>

#include "esp_err.h"
#include "esp_log.h"    // Incluindo a biblioteca de log
#include "esp_system.h" // Incluindo a biblioteca de configuração do sistema

#include "esp_spiffs.h" // Incluindo a biblioteca de SPIFFS
#include "nvs.h"
#include "nvs_flash.h"

static const char *StorageTAG = "storage_manager"; // Tag para Log

esp_err_t open_nvs_handle(nvs_handle_t *my_handle, int mode) {
    ESP_LOGI(StorageTAG, "Opening Non-Volatile Storage (NVS) handle... ");

    esp_err_t ret = nvs_open("storage", mode, my_handle);
    if (ret != ESP_OK) {
        ESP_LOGI(StorageTAG, "Error (%s) opening NVS handle!",
                 esp_err_to_name(ret));
        return ret;
    } else {
        ESP_LOGI(StorageTAG, "Done");
    }

    return ESP_OK;
}

void init_storage_manager(void) {
    ESP_LOGI(StorageTAG, "Iniciando NVS");

    esp_err_t ret = nvs_flash_init(); // Inicialização do armazenamento de dados
                                      // persistentes
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES ||
        ret == ESP_ERR_NVS_NEW_VERSION_FOUND) { // Verificação de erro
        ESP_ERROR_CHECK(nvs_flash_erase()); // Erro de erase do armazenamento de
                                            // dados persistentes
        ret = nvs_flash_init(); // Inicialização do armazenamento de dados
                                // persistentes
    }
    ESP_ERROR_CHECK(ret); // Verificação de erro

    nvs_handle_t my_handle;

    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READWRITE));

    int32_t boots = 0; // Variável que armazena o número de boots

    nvs_get_i32(my_handle, "boots", &boots); // Recupera o número de boots

    if (boots == 0) { // Verificação de erro
        ESP_LOGI(StorageTAG, "Contador de boots não encontrado");
    } else {
        boots++; // Incrementa o número de boots
        ESP_LOGI(StorageTAG, "Boots: %ld", boots); // Imprime o número de boots
        nvs_set_i32(my_handle, "boots", boots); // Armazena o número de boots
    }

    nvs_commit(my_handle); // Armazena as alterações no storage

    nvs_close(
        my_handle); // Fecha o handle do armazenamento de dados persistentes
}

bool storage_manager_is_first_boot() {
    nvs_handle_t my_handle; // Handle para o NVS
    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READONLY));

    int32_t boots = 0; // Numero de boot do sistema

    nvs_get_i32(my_handle, "boots",
                &boots); // Lê o número de vezes que o sistema foi iniciado

    nvs_close(my_handle); // Fecha o handle

    if (boots == 0) { // Se o número de boot for igual a 0, então é a primeira
                      // vez que o sistema é iniciado
        return true; // Retorna true
    }
    // Se não for a primeira vez que o sistema é iniciado
    return false; // Retorna false
}

void restore_default_config(bool is_first_boot) {

    nvs_handle_t my_handle;
    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READWRITE));

    nvs_set_str(my_handle, "ap_ssid", "Monitoramento de ambiente");
    nvs_set_str(my_handle, "ap_password", "1234567890");
    nvs_set_str(my_handle, "wifi_ssid", ".");
    nvs_set_str(my_handle, "wifi_Password", ".");
    nvs_set_str(my_handle, "eap_password", ".");
    nvs_set_str(my_handle, "eap_identify", ".");
    nvs_set_str(my_handle, "mqtt_server_uri", "mqtt://broker.hivemq.com");
    nvs_set_str(my_handle, "mqtt_user", "admin");
    nvs_set_str(my_handle, "mqtt_password", "admin");
    nvs_set_str(my_handle, "server_user", "admin");
    nvs_set_str(my_handle, "server_password", "admin");
    nvs_set_i8(my_handle, "wifi_mode",
               1); // 2 = Modo STA ENTERPRISE, 1 = Modo AP, 0 = Modo STA
    nvs_set_str(my_handle, "update_url",
                "http://embarcacoes.ic.unicamp.br/updates/"
                "monitoramento-de-ambiente-esp32s2-latest.bin");
    nvs_set_str(my_handle, "mqtt_topic", "device/3/");
    nvs_set_i8(my_handle, "auto_ap_init", 1);

    if (is_first_boot == true) {
        nvs_set_i32(my_handle, "boots", 1);
    }

    ESP_ERROR_CHECK(nvs_commit(my_handle));

    nvs_close(my_handle);
}

void read_value_from_nvs(char *key, void *pointer, data_type_t type) {
    nvs_handle_t my_handle;
    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READONLY));

    size_t len;

    if (type == INT64) {
        nvs_get_i64(my_handle, key, (int64_t *)pointer);
    } else if (type == INT32) {
        nvs_get_i32(my_handle, key, (int32_t *)pointer);
    } else if (type == INT16) {
        nvs_get_i16(my_handle, key, (int16_t *)pointer);
    } else if (type == INT8) {
        nvs_get_i8(my_handle, key, (int8_t *)pointer);
    } else if (type == UINT64) {
        nvs_get_u64(my_handle, key, (uint64_t *)pointer);
    } else if (type == UINT32) {
        nvs_get_u32(my_handle, key, (uint32_t *)pointer);
    } else if (type == UINT16) {
        nvs_get_u16(my_handle, key, (uint16_t *)pointer);
    } else if (type == UINT8) {
        nvs_get_u8(my_handle, key, (uint8_t *)pointer);
    } else if (type == STR) {
        nvs_get_str(my_handle, key, NULL, &len);
        nvs_get_str(my_handle, key, (char *)pointer, &len);
    }

    nvs_close(my_handle);
}

void read_string_size_from_nvs(char *key, size_t *len) {
    nvs_handle_t my_handle;
    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READONLY));

    nvs_get_str(my_handle, key, NULL, len);

    nvs_close(my_handle);
}

void write_value_in_nvs(char *key, void *pointer, data_type_t type) {
    nvs_handle_t my_handle;
    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READWRITE));

    if (type == INT64) {
        nvs_set_i64(my_handle, key, *((int64_t *)pointer));
    } else if (type == INT32) {
        nvs_set_i32(my_handle, key, *((int32_t *)pointer));
    } else if (type == INT16) {
        nvs_set_i16(my_handle, key, *((int16_t *)pointer));
    } else if (type == INT8) {
        nvs_set_i8(my_handle, key, *((int8_t *)pointer));
    } else if (type == UINT64) {
        nvs_set_u64(my_handle, key, *((uint64_t *)pointer));
    } else if (type == UINT32) {
        nvs_set_u32(my_handle, key, *((uint32_t *)pointer));
    } else if (type == UINT16) {
        nvs_set_u16(my_handle, key, *((uint16_t *)pointer));
    } else if (type == UINT8) {
        nvs_set_u8(my_handle, key, *((uint8_t *)pointer));
    } else if (type == STR) {
        nvs_set_str(my_handle, key, (char *)pointer);
    }

    nvs_commit(my_handle);

    nvs_close(my_handle);
}

void write_i64_in_nvs(char *key, int64_t value) {
    nvs_handle_t my_handle;
    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READWRITE));

    nvs_set_i64(my_handle, key, value);

    ESP_ERROR_CHECK(nvs_commit(my_handle));

    nvs_close(my_handle);
}

void write_i32_in_nvs(char *key, int32_t value) {
    nvs_handle_t my_handle;
    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READWRITE));

    nvs_set_i32(my_handle, key, value);

    ESP_ERROR_CHECK(nvs_commit(my_handle));

    nvs_close(my_handle);
}

void write_i16_in_nvs(char *key, int16_t value) {
    nvs_handle_t my_handle;
    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READWRITE));

    nvs_set_i16(my_handle, key, value);

    ESP_ERROR_CHECK(nvs_commit(my_handle));

    nvs_close(my_handle);
}

void write_i8_in_nvs(char *key, int8_t value) {
    nvs_handle_t my_handle;
    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READWRITE));

    nvs_set_i8(my_handle, key, value);

    ESP_ERROR_CHECK(nvs_commit(my_handle));

    nvs_close(my_handle);
}

void write_u64_in_nvs(char *key, uint64_t value) {
    nvs_handle_t my_handle;
    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READWRITE));

    nvs_set_u64(my_handle, key, value);

    ESP_ERROR_CHECK(nvs_commit(my_handle));

    nvs_close(my_handle);
}

void write_u32_in_nvs(char *key, uint32_t value) {
    nvs_handle_t my_handle;
    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READWRITE));

    nvs_set_u32(my_handle, key, value);

    ESP_ERROR_CHECK(nvs_commit(my_handle));

    nvs_close(my_handle);
}

void write_u16_in_nvs(char *key, uint16_t value) {
    nvs_handle_t my_handle;
    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READWRITE));

    nvs_set_u16(my_handle, key, value);

    ESP_ERROR_CHECK(nvs_commit(my_handle));

    nvs_close(my_handle);
}

void write_u8_in_nvs(char *key, uint8_t value) {
    nvs_handle_t my_handle;
    ESP_ERROR_CHECK(open_nvs_handle(&my_handle, NVS_READWRITE));

    nvs_set_u8(my_handle, key, value);

    ESP_ERROR_CHECK(nvs_commit(my_handle));

    nvs_close(my_handle);
}
