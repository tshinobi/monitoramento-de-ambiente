/**
 * @file storage_manager.h
 * @author Julio Nunes Avelar (julio@bzoide.dev)
 * @brief
 * @version 0.1
 * @date 2023-09-23
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef __STORAGE_MANAGER__H__
#define __STORAGE_MANAGER__H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define MAX_LINE_IN_FILE CONFIG_MAX_LINE_IN_FILE
#define DATA_PATH CONFIG_DATA_PATH

typedef enum data_type_t {
    INT64 = 1,
    INT32,
    INT16,
    INT8,
    UINT64,
    UINT32,
    UINT16,
    UINT8,
    STR,
} data_type_t;

void init_storage_manager(void);

bool storage_manager_is_first_boot();

void restore_default_config(bool is_first_boot);

void read_value_from_nvs(char *key, void *pointer, data_type_t type);

void read_string_size_from_nvs(char *key, size_t *len);

void write_value_in_nvs(char *key, void *pointer, data_type_t type);

void write_i64_in_nvs(char *key, int64_t value);
void write_i32_in_nvs(char *key, int32_t value);
void write_i16_in_nvs(char *key, int16_t value);
void write_i8_in_nvs(char *key, int8_t value);
void write_u64_in_nvs(char *key, uint64_t value);
void write_u32_in_nvs(char *key, uint32_t value);
void write_u16_in_nvs(char *key, uint16_t value);
void write_u8_in_nvs(char *key, uint8_t value);

#ifdef __cplusplus
}
#endif

#endif //!__STORAGE_MANAGER__H__