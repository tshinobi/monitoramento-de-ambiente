/**
 * @file payload_manager.c
 * @author Julio Nunes Avelar (julio@bzoide.dev)
 * @brief
 * @version 0.1
 * @date 2023-09-23
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "payload_manager.h"
#include <math.h>   // Biblioteca padrão
#include <stdio.h>  // Biblioteca padrão
#include <stdlib.h> // Biblioteca padrão
#include <string.h>

#include "freertos/FreeRTOS.h" // Biblioteca de sistema de tarefas
#include "freertos/task.h"     // Biblioteca de tarefas

#include "esp_err.h" // Biblioteca de erros
#include "esp_log.h" // Biblioteca de log
#include "esp_system.h"

#include "DHT.h"
#include "bh1750.h"
#include "bmx280.h"
#include "driver/i2c.h"

static const char *SensorsTAG = "Payload_Manager"; // Tag usada para logs

static bh1750_handle_t bh1750 = NULL;
static bmx280_t *bmx280 = NULL;

void init_i2c() {
    ESP_LOGI(SensorsTAG, "Iniciando I2C");

    i2c_config_t i2c_cfg = {.mode = I2C_MODE_MASTER,
                            .sda_io_num = SDA_GPIO,
                            .scl_io_num = SCL_GPIO,
                            .sda_pullup_en = false,
                            .scl_pullup_en = false,

                            .master = {.clk_speed = I2C_MASTER_FREQ_HZ}};

    ESP_ERROR_CHECK(i2c_param_config(I2C_MASTER_NUM, &i2c_cfg));
    ESP_ERROR_CHECK(
        i2c_driver_install(I2C_MASTER_NUM, I2C_MODE_MASTER, 0, 0, 0));

    ESP_LOGI(SensorsTAG, "I2C iniciada com sucesso");
}

void stop_i2c() { ESP_ERROR_CHECK(i2c_driver_delete(I2C_MASTER_NUM)); }

void init_bh1750() {
    bh1750 = bh1750_create(I2C_MASTER_NUM, BH1750_I2C_ADDRESS_DEFAULT);
}

void delete_bh1750() { ESP_ERROR_CHECK(bh1750_delete(bh1750)); }

void bh1750_set_mode(int mode) {
    bh1750_measure_mode_t cmd_measure;
    if (mode == BH1750_CONTINUOS_MODE) {
        ESP_LOGI(SensorsTAG, "Configurando BH1750 no modo de leitura continua");
        // continous mode
        cmd_measure = BH1750_CONTINUE_4LX_RES;
    } else {
        ESP_LOGI(SensorsTAG, "Configurando BH1750 no modo one-shot");
        // one-shot mode
        cmd_measure = BH1750_ONETIME_4LX_RES;
    }

    ESP_ERROR_CHECK(bh1750_set_measure_mode(bh1750, cmd_measure));
    vTaskDelay(30 / portTICK_PERIOD_MS);
}

void read_bh1750(float *lux) { ESP_ERROR_CHECK(bh1750_get_data(bh1750, lux)); }

void init_bme280() {
    bmx280 = bmx280_create(I2C_MASTER_NUM);

    if (!bmx280) {
        ESP_LOGE(SensorsTAG, "Não foi possível criar o driver bmx280.");
        return;
    }

    ESP_ERROR_CHECK(bmx280_init(bmx280));

    bmx280_config_t bmx_cfg = BMX280_DEFAULT_CONFIG;
    ESP_ERROR_CHECK(bmx280_configure(bmx280, &bmx_cfg));
}

void bme280_set_mode(int mode) {
    bmx280_mode_t modo;

    if (mode == BME280_FORCE_MODE) {
        modo = BMX280_MODE_FORCE;
    } else if (mode == BME280_CYCLE_MODE) {
        modo = BMX280_MODE_CYCLE;
    } else {
        modo = BMX280_MODE_SLEEP;
    }

    ESP_ERROR_CHECK(bmx280_setMode(bmx280, modo));
}

void read_bme280(float *temp, float *hum, float *pres) {
    ESP_ERROR_CHECK(bmx280_readoutFloat(bmx280, temp, pres, hum));
}

void init_dht() { setDHTgpio(DHT_PIN); }

void read_dht(double *temperature, double *humidity) {
    readDHT();
    *humidity = getHumidity();
    *temperature = getTemperature();
}
