#ifndef __WEB_SERVER__H__
#define __WEB_SERVER__H__

#ifdef __cplusplus
extern "C" {
#endif

#include "cJSON.h"
#include "esp_netif.h"
#include <esp_log.h>
#include <esp_system.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <esp_https_server.h>

#include "communication.h"
#include "storage_manager.h"
#include "time_manager.h"

#include <mbedtls/sha512.h>

#define SCRATCH_BUFSIZE (10240)

#define WebServerTAG "web_server"

static char token[129];

static time_t token_time;

// API
void generate_token();
bool check_token(char *token_received);
esp_err_t handleAPILogin(httpd_req_t *req);
esp_err_t handleAPIConfigs(httpd_req_t *req);
esp_err_t handleAPIConfig(httpd_req_t *req);
esp_err_t handleAPIChangeSecrets(httpd_req_t *req);

// System
esp_err_t handleReboot(httpd_req_t *req);
esp_err_t handleQuit(httpd_req_t *req);
esp_err_t handleInitAPmode(httpd_req_t *req);
esp_err_t handleInitSTAmode(httpd_req_t *req);
esp_err_t handleInitSTAENTmode(httpd_req_t *req);
esp_err_t handleReconfig(httpd_req_t *req);

// Main
esp_err_t init_web_server();
esp_err_t stop_web_server();

#ifdef __cplusplus
}
#endif

#endif //!__WEB_SERVER__H__