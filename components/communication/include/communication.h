/**
 * @file communication.h
 * @author Julio Nunes Avelar (julio@bzoide.dev)
 * @brief Arquivo de cabeçalho de biblioteca para abstrair implementação do WiFi
 * e do MQTT do esp32
 * @version 0.1
 * @date 2023-09-23
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef __COMMUNICATION__H__
#define __COMMUNICATION__H__

#include <stdint.h>
#ifdef __cplusplus
extern "C" {
#endif

#include "esp_err.h"
#include "sdkconfig.h"

// Variaveis e objetos globais

// #define CONFIG_ESP_WIFI_AUTH_WPA2_PSK 1

#if CONFIG_ESP_WIFI_AUTH_OPEN
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_OPEN
#elif CONFIG_ESP_WIFI_AUTH_WEP
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WEP
#elif CONFIG_ESP_WIFI_AUTH_WPA_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA_PSK
#elif CONFIG_ESP_WIFI_AUTH_WPA2_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA2_PSK
#elif CONFIG_ESP_WIFI_AUTH_WPA_WPA2_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA_WPA2_PSK
#elif CONFIG_ESP_WIFI_AUTH_WPA3_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA3_PSK
#elif CONFIG_ESP_WIFI_AUTH_WPA2_WPA3_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA2_WPA3_PSK
#elif CONFIG_ESP_WIFI_AUTH_WAPI_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WAPI_PSK
#endif

#define WIFI_CONNECTED_BIT BIT0 // Bit 0 - Wifi conectado
#define WIFI_FAIL_BIT BIT1      // Bit 1 - Falha na conexão
#define maximum_retry                                                          \
    CONFIG_MAXIMUM_RETRY // Quantidade máxima de tentativas de conexão

// Macros para escolher o modo de operação do radio wifi

#define WIFI_AP_MODE 1         // Modo AP
#define WIFI_STATION_MODE 0    // Modo Station
#define WIFI_ENTERPRISE_MODE 2 // Modo enterprise
#define WIFI_STA_MODE WIFI_STATION_MODE
#define WIFI_EAP_MODE WIFI_ENTERPRISE_MODE
#define START_AP_ON_STA_FAIL true
#define NOT_START_AP_ON_STA_FAIL false

#define ENABLE_VALIDATE_SERVER_CERT                                            \
    CONFIG_ENABLE_VALIDATE_SERVER_CERT // Habilita verificação de certificado

#define DEVICE_HOSTNAME CONFIG_DEVICE_HOSTNAME

#define WIFI_MAX_CONNECTIONS CONFIG_WIFI_MAX_CONNECTIONS
#define WIFI_AP_CHANNEL CONFIG_WIFI_AP_CHANNEL

// Tipo de dado abstrato de configuração do WiFi
typedef struct {
    int8_t wifi_mode;          // Modo do Wi-Fi
    int8_t auto_ap_init;       // Iniciar AP automaticamente ao não conseguir
                               // conectar wifi
    char ap_ssid[32];          // SSID do AP
    char ap_password[64];      // Senha do AP
    char ssid[32];             // SSID da rede
    char password[64];         // Senha da rede
    char mqtt_server_uri[128]; // Endereço do servidor MQTT
    char mqtt_user[32];        // Usuário do servidor MQTT
    char mqtt_password[64];    // Senha do servidor MQTT
    char mqtt_topic[128];      // Tópico do servidor MQTT

    char eap_identify[128]; // Identificação do WiFi Enterprise
    char eap_password[128]; // Senha do WiFi Enterprise
} communication_config_t;

/**
 * @brief Função responsavel por iniciar a comunicação via wifi.
 *
 */
void init_wifi(void);

/**
 * @brief Função responsavel por setar o callback do MQTT.
 *
 * @param _callback Ponteiro para função de callback.
 */
void setcallback(void (*_callback)(char *, char *, unsigned int, unsigned int));

/**
 * @brief // Função responsavel por iniciar o MQTT.
 *
 */
void mqtt_start(void);

/**
 * @brief Função responsavel por enviar dados via MQTT
 *
 * @param message payload a ser enviado
 * @return int id de erro da mensagem enviada, 0 e sucesso.
 */
int send_message(char message[]);

/**
 * @brief Função responsavel por parar o MQTT
 *
 * @return esp_err_t Resultado ao parar MQTT
 */
esp_err_t stop_mqtt(void); // Função para parar o MQTT

/**
 * @brief Função responsavel por configurar o modulo de comunicação
 *
 * @param config ponteiro para otipo de dado abstrato do tipo
 * communication_config com as configurações
 */
void config_communication(communication_config_t *config);

/**
 * @brief Função responsavel por obter o endereço IP do WiFi
 *
 * @param ip_address ponteiro para o buffer onde sera salvo o endereço IP
 */
void get_ip_address(unsigned char *ip_address);

/**
 * @brief Função responsavel por obter o endereço MAC do Dispositivo
 *
 * @param mac_address ponteiro para o buffer onde sera salvo o endereço MAC
 * @return esp_err_t Resultado da operação
 */
esp_err_t get_mac_address(unsigned char *mac_address);

/**
 * @brief Retorna a configuração
 *
 * @return communication_config_t* ponteiro para configuração
 */
communication_config_t *get_config();

#ifdef CONFIG_ENABLE_STORAGE_MANAGER_INTEGRATION
/**
 * @brief Configurar o modulo de comunicação com base nos dados salvos na
 * NVS
 *
 * @param config Ponteiro para um struct de configuração
 */
communication_config_t *config_communication_by_nvs();
#endif
#ifdef __cplusplus
}
#endif

#endif //!__COMMUNICATION__H__