/**
 * @file communication.h
 * @author Julio Nunes Avelar (julio@bzoide.dev)
 * @brief Arquivo de implementação de biblioteca para abstrair implementação do
 * WiFi e do MQTT do esp32
 * @version 0.1
 * @date 2023-09-23
 *
 * @copyright Copyright (c) 2023
 *
 */
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "freertos/FreeRTOS.h"     // FreeRTOS
#include "freertos/event_groups.h" // Event Groups
#include "freertos/projdefs.h"
#include "freertos/task.h" // Task

#include "esp_event.h" // Biblioteca de Eventos
#include "esp_log.h"   // Biblioteca de Log
#include "esp_mac.h"
#include "esp_netif.h"
#include "esp_system.h" // ESP32
#include "esp_wifi.h"   // Biblioteca do Wifi
#include "esp_wpa2.h"

#include "communication.h" // Incluindo o arquivo de cabeçalho
#include "esp_mac.h"
#include "esp_netif.h"
#include "esp_wifi_types.h"
#include "mqtt_client.h" // Biblioteca MQTT
#include "multi_heap.h"
#include "nvs.h"

#ifdef CONFIG_ENABLE_STORAGE_MANAGER_INTEGRATION
#include "nvs_flash.h"
#include "storage_manager.h"
#endif

static const char *CommunicationTAG = "Comunicação";

static EventGroupHandle_t s_wifi_event_group;

esp_mqtt_client_handle_t global_client; // Cliente MQTT

int s_retry_num = 0; // Contador de tentativas de conexão

void (*mqtt_callback)(char *, char *, unsigned int,
                      unsigned int); // Função de callback

communication_config_t *sys_config;

#ifdef ENABLE_VALIDATE_SERVER_CERT
extern uint8_t ca_pem_start[] asm("_binary_ca_pem_start");
extern uint8_t ca_pem_end[] asm("_binary_ca_pem_end");
#endif /* CONFIG_EXAMPLE_VALIDATE_SERVER_CERT */

/**
 * @brief Função responsavel por tratar os eventos do wifi
 *
 * @param arg Argumetos extras do evento
 * @param event_base Tipo do evento
 * @param event_id ID do evento
 * @param event_data Dados do evento
 */
void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id,
                   void *event_data) {
    if (event_base == WIFI_EVENT &&
        event_id ==
            WIFI_EVENT_STA_START) { // Verifica se o evento e de WiFi é se e do
                                    // tipo de inicialização de estação
        esp_wifi_connect();

    } else if (event_base == WIFI_EVENT &&
               event_id ==
                   WIFI_EVENT_STA_DISCONNECTED) { // Verifica se o evento e de
                                                  // WiFi é se do tipo de
                                                  // desconexão

        if (s_retry_num <
            maximum_retry) { // Verifica se o numero de tentativas de reconexã
                             // ainda não atingiu o limite
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(CommunicationTAG,
                     "Tentando conectar ao ponto de acesso"); // Log de debug
        } else { // Caso contrario toma as medidas necessarias
            ESP_LOGE(
                CommunicationTAG,
                "Não foi possível conectar ao ponto de acesso"); // Log de debug
            if (sys_config->auto_ap_init) {
                ESP_LOGI(CommunicationTAG, "Iniciando modo AP");

                write_i8_in_nvs("wifi_mode", 1);

                vTaskDelay(pdMS_TO_TICKS(30));

                esp_restart();
            }

            xEventGroupSetBits(s_wifi_event_group,
                               WIFI_FAIL_BIT); // Envia um sinal de falha e o
                                               // dispositivo reinicia
        }
        ESP_LOGI(CommunicationTAG, "connect to the AP fail");

    } else if (event_base == IP_EVENT &&
               event_id == IP_EVENT_STA_GOT_IP) { // Verifica se o evento de
                                                  // obtenção de IP
        ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;

        ESP_LOGI(CommunicationTAG, "Obteve o IP:" IPSTR,
                 IP2STR(&event->ip_info.ip));

        s_retry_num = 0; // Coloca o numero de tentativas como 0, pois a conexão
                         // foi estabelecida

        xEventGroupSetBits(s_wifi_event_group,
                           WIFI_CONNECTED_BIT); // Envia um sinal de conexão

    } else if (event_id ==
               WIFI_EVENT_AP_STACONNECTED) { // Evento para quando o ESP está no
                                             // modo AP e algum dispositivo se
                                             // conecta a ele

        wifi_event_ap_staconnected_t *event =
            (wifi_event_ap_staconnected_t *)event_data;

        ESP_LOGI(CommunicationTAG, "station " MACSTR " join, AID=%d",
                 MAC2STR(event->mac), event->aid);

    } else if (event_id ==
               WIFI_EVENT_AP_STADISCONNECTED) { // Evento para quando o ESP está
                                                // no modo AP e algum
                                                // dispositivos se desconecta
                                                // dele

        wifi_event_ap_stadisconnected_t *event =
            (wifi_event_ap_stadisconnected_t *)event_data;

        ESP_LOGI(CommunicationTAG, "station " MACSTR " leave, AID=%d",
                 MAC2STR(event->mac), event->aid);
    }
}

/**
 * @brief Função responsavel por iniciar o wifi
 *
 */
void init_wifi(void) {
    s_wifi_event_group =
        xEventGroupCreate(); // Cria o gerenciador de eventos globais do WiFi

    ESP_ERROR_CHECK(esp_netif_init()); // Inicia o netif

    ESP_ERROR_CHECK(esp_event_loop_create_default());

    esp_netif_t *default_net_if = NULL;

    if (sys_config->wifi_mode ==
        WIFI_AP_MODE) { // Se o modo do Wi-Fi for igual ao modo AP
        default_net_if = esp_netif_create_default_wifi_ap(); // Cria o Wi-Fi AP
    } else { // Se não sera do modo STA
        default_net_if = esp_netif_create_default_wifi_sta();
    }

    ESP_ERROR_CHECK(
        esp_netif_set_hostname(default_net_if,
                               DEVICE_HOSTNAME)); // Altera o hostname do ESP

    wifi_init_config_t cfg =
        WIFI_INIT_CONFIG_DEFAULT(); // Inicia uma configuração padrão do WiFi
    ESP_ERROR_CHECK(esp_wifi_init(&cfg)); // Inicia o wifi com essa configuração

    esp_event_handler_instance_t
        instance_any_id; // Cria um manajer para eventos diversos do WiFi
    esp_event_handler_instance_t
        instance_got_ip; // Cria um manajer para eventos para lidar com eventos
                         // de endereço IP

    ESP_ERROR_CHECK(esp_event_handler_instance_register(
        WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL, &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(
        IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL, &instance_got_ip));

    ESP_LOGI(CommunicationTAG, "Configurando WiFi"); // Log de debug

    if (sys_config->wifi_mode ==
        WIFI_AP_MODE) { // Se o WiFi for iniciar no modo AP

        // Configura ssd e senha, além de outros parametros
        wifi_config_t wifi_config = {
            .ap =
                {
                    .ssid_len = strlen(sys_config->ap_ssid),
                    .max_connection = WIFI_MAX_CONNECTIONS,
                    .authmode = WIFI_AUTH_WPA_WPA2_PSK,
                    .channel = WIFI_AP_CHANNEL,
                },
        };
        if (strlen(sys_config->ap_password) ==
            0) { // Caso não tenha senha, configuro a autenticação como aberta
            wifi_config.ap.authmode = WIFI_AUTH_OPEN;
        }

        // Configura o SSID e a Senha do WiFi AP
        strlcpy((char *)wifi_config.ap.ssid, sys_config->ap_ssid,
                sizeof(wifi_config.ap.ssid)); // Configurações do Wi-Fi AP
        strlcpy((char *)wifi_config.ap.password, sys_config->ap_password,
                sizeof(wifi_config.ap.password)); // Configurações do Wi-Fi AP

        // Finaliza as configurações do WiFi AP
        ESP_ERROR_CHECK(
            esp_wifi_set_mode(WIFI_MODE_AP)); // Define o modo do Wi-Fi
        ESP_ERROR_CHECK(esp_wifi_set_config(
            WIFI_IF_AP, &wifi_config)); // Define as configurações do Wi-Fi

        ESP_LOGI(CommunicationTAG,
                 "wifi_init_softap finished. SSID:%s channel:%d",
                 sys_config->ap_ssid, 1);

    } else {
        wifi_config_t wifi_config = {};

        strlcpy((char *)wifi_config.sta.ssid, sys_config->ssid,
                sizeof(wifi_config.sta.ssid)); // Define o SSID do Wi-Fi
        strlcpy((char *)wifi_config.sta.password, sys_config->password,
                sizeof(wifi_config.sta.password)); // Define a senha do Wi-Fi

        if (sys_config->wifi_mode ==
            WIFI_ENTERPRISE_MODE) { // Caso o WiFi sejá do tipo enterprise
            ESP_ERROR_CHECK(esp_wifi_set_storage(
                WIFI_STORAGE_RAM)); // Altero o local de armazenamento para RAM

        } else {
            wifi_config.sta.threshold.authmode =
                ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD; // Configuro o tipo de
                                                   // autenticação da rede que
                                                   // será conectado

            wifi_config.sta.sae_pwe_h2e = WPA3_SAE_PWE_BOTH;

            if (strlen(sys_config->password) ==
                0) { // Caso não tenha senha configuro a rede como aberta
                wifi_config.sta.threshold.authmode = WIFI_AUTH_OPEN;
            }
        }

        // Finalizo as configurações do WiFi

        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
        ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));

        if (sys_config->wifi_mode ==
            WIFI_ENTERPRISE_MODE) { // Copio as informações de autenticação do
                                    // WiFi Enterprise

            ESP_ERROR_CHECK(esp_wifi_sta_wpa2_ent_set_identity(
                (uint8_t *)"anonimous", strlen("anonimous")));
            ESP_ERROR_CHECK(esp_wifi_sta_wpa2_ent_set_username(
                (uint8_t *)sys_config->eap_identify,
                strlen(sys_config->eap_identify)));
            ESP_ERROR_CHECK(esp_wifi_sta_wpa2_ent_set_password(
                (uint8_t *)sys_config->eap_password,
                strlen(sys_config->eap_password)));
            ESP_ERROR_CHECK(esp_wifi_sta_wpa2_ent_set_ttls_phase2_method(3));

#if defined(ENABLE_VALIDATE_SERVER_CERT) // Caso a verificação de certificado
                                         // esteja habilitada
            unsigned int ca_pem_bytes = ca_pem_end - ca_pem_start;
            ESP_ERROR_CHECK(
                esp_wifi_sta_wpa2_ent_set_ca_cert(ca_pem_start, ca_pem_bytes));
#endif
            ESP_ERROR_CHECK(esp_wifi_sta_wpa2_ent_enable());
        }
    }
    ESP_LOGI(CommunicationTAG,
             "Configuração do WiFi terminada"); // Log de debug

    // Inicio o Wifi

    ESP_ERROR_CHECK(esp_wifi_start());

    // Esperando até que a conexão seja estabelecida (WIFI_CONNECTED_BIT)
    // ou a conexão falhou no número máximo de tentativas (WIFI_FAIL_BIT).
    // Os bits são definidos por event_handler() (veja acima)
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
                                           WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
                                           pdFALSE, pdFALSE, portMAX_DELAY);

    // xEventGroupWaitBits() retorna os bits antes do retorno da chamada,
    // portanto pode testar qual evento realmente aconteceu.
    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(CommunicationTAG, "conectado a rede com SSID:%s",
                 sys_config->ssid);
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(CommunicationTAG, "Falha ao tentar conectar ao SSID:%s",
                 sys_config->ssid);

    } else {
        ESP_LOGE(CommunicationTAG, "Evento inesperado");
    }
}

/**
 * @brief Função responsavel por setar a função de callback para o MQTT
 *
 * @param _callback // Ponteiro pra função de Callback
 */
void setcallback(
    void (*_callback)(char *, char *, unsigned int,
                      unsigned int)) { // Função para definir o callback de
                                       // recebimento de mensagem)

    mqtt_callback = _callback; // Define o callback de recebimento de mensagem
}

/**
 * @brief Função para log de erro, caso o erro sejá diferente de 0, siginifica
 * que falhou
 *
 * @param message Mensagem do erro
 * @param error_code codigo do erro
 */
void log_error_if_nonzero(const char *message, int error_code) {
    if (error_code != 0) { // Se o erro não for zero significa que teve falha

        ESP_LOGE(CommunicationTAG, "Último erro %s: 0x%x", message,
                 error_code); // Log de debug com o erro e o código
    }
}

/**
 * @brief Função responsavel por tratar os eventos do MQTT
 *
 * @param handler_args Argumetos extras do evento
 * @param event_base Tipo do evento
 * @param event_id ID do evento
 * @param event_data Dados do evento
 */
void mqtt_event_handler(void *handler_args, esp_event_base_t base,
                        int32_t event_id, void *event_data) {
    // ESP_LOGD(CommunicationTAG, "Event dispatched from event loop base=%s,
    // event_id=%d", base, event_id);
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    switch ((esp_mqtt_event_id_t)event_id) { // Verifica o tipo de evento
    case MQTT_EVENT_CONNECTED:               // Conexão realizada
        ESP_LOGI(CommunicationTAG, "MQTT_EVENT_CONNECTED");

        msg_id = esp_mqtt_client_publish(
            client, sys_config->mqtt_topic, "conectado", 0, 1,
            0); // Envia uma mensagem para informar a conexão
        ESP_LOGI(CommunicationTAG, "mensagem publicada com sucesso, msg_id=%d",
                 msg_id);

        char temp_mqtt_topic[148]; // Buffer para armazenar o topico final

        strlcpy(temp_mqtt_topic, sys_config->mqtt_topic,
                sizeof(temp_mqtt_topic)); // Copio o topico da configuração para
                                          // o buffer temporario
        strlcat(temp_mqtt_topic, "receive/",
                sizeof(temp_mqtt_topic) - 1); // concateno receive/ no final

        msg_id = esp_mqtt_client_subscribe(client, temp_mqtt_topic,
                                           0); // Subscrição ao topico
        ESP_LOGI(CommunicationTAG, "enviado inscrição com sucesso, msg_id=%d",
                 msg_id); // Log de debug com o id da mensagem

        break;
    case MQTT_EVENT_DISCONNECTED:
        ESP_LOGI(CommunicationTAG, "MQTT_EVENT_DISCONNECTED");

        break;

    case MQTT_EVENT_SUBSCRIBED:
        ESP_LOGI(CommunicationTAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d",
                 event->msg_id); // Log de debug com o id da mensagem
        msg_id =
            esp_mqtt_client_publish(client, sys_config->mqtt_topic, "conectado",
                                    0, 0, 0); // Publica mensagem no topico
        ESP_LOGI(CommunicationTAG, "sent publish successful, msg_id=%d",
                 msg_id); // Log de debug com o id da mensagem
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGI(CommunicationTAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d",
                 event->msg_id);
        break;
    case MQTT_EVENT_PUBLISHED:
        ESP_LOGI(CommunicationTAG, "MQTT_EVENT_PUBLISHED, msg_id=%d",
                 event->msg_id);
        break;
    case MQTT_EVENT_DATA:
        if (!mqtt_callback) {
            ESP_LOGI(CommunicationTAG, "MQTT_EVENT_DATA");
            printf("TOPIC=%.*s\r\n", event->topic_len,
                   event->topic); // Log de debug com o topico
            printf("DATA=%.*s\r\n", event->data_len,
                   event->data); // Log de debug com o payload
        } else {
            mqtt_callback(
                event->topic, event->data, event->topic_len,
                event->data_len); // Chama o callback de recebimento de mensagem
        }
        break;
    case MQTT_EVENT_ERROR:
        ESP_LOGI(CommunicationTAG, "MQTT_EVENT_ERROR");
        if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT) {
            log_error_if_nonzero("reported from esp-tls",
                                 event->error_handle->esp_tls_last_esp_err);
            log_error_if_nonzero("reported from tls stack",
                                 event->error_handle->esp_tls_stack_err);
            log_error_if_nonzero("captured as transport's socket errno",
                                 event->error_handle->esp_transport_sock_errno);
            ESP_LOGI(CommunicationTAG, "Last errno string (%s)",
                     strerror(event->error_handle->esp_transport_sock_errno));
        }
        break;
    default:
        ESP_LOGI(CommunicationTAG, "Erro desconhecido, id do erro:%d",
                 event->event_id);
        break;
    }
}

/**
 * @brief // Função responsavel por iniciar o MQTT.
 *
 */
void mqtt_start(void) // Inicia o MQTT
{
    if (sys_config->wifi_mode == WIFI_AP_MODE)
        return;

    ESP_LOGI(CommunicationTAG, "Configurando MQTT"); // Log de debug

    ESP_LOGI(CommunicationTAG, "MQTT: conectando ao host %s",
             sys_config->mqtt_server_uri); // Log de debug

    const esp_mqtt_client_config_t mqtt_cfg = {
        .broker.address.uri = sys_config->mqtt_server_uri, // URI do servidor
        .credentials.username = sys_config->mqtt_user, // Usuário do servidor
        .credentials.authentication.password =
            sys_config->mqtt_password, // Senha do servidor
    };                                 // Configurações do MQTT

    global_client = esp_mqtt_client_init(&mqtt_cfg); // Configura o cliente MQTT

    ESP_LOGI(CommunicationTAG,
             "Configuração do MQTT terminada"); // Log de debug

    esp_mqtt_client_register_event(global_client, ESP_EVENT_ANY_ID,
                                   mqtt_event_handler,
                                   NULL); // Registra o callback de eventos
    esp_mqtt_client_start(global_client); // Inicia o cliente MQTT

    ESP_LOGI(CommunicationTAG, "MQTT, iniciado");
}

/**
 * @brief Função responsavel por enviar dados via MQTT
 *
 * @param message payload a ser enviado
 * @return int id de erro da mensagem enviada, 0 e sucesso.
 */
int send_message(char *message) {
    if (sys_config->wifi_mode == WIFI_AP_MODE)
        return -1;
    // Envia mensagem
    ESP_LOGI(CommunicationTAG, "Enviando mensagem ao topico %s: %s",
             sys_config->mqtt_topic, message); // Log de debug

    int msg_id = esp_mqtt_client_publish(global_client, sys_config->mqtt_topic,
                                         message, 0, 0, 0); // Envia a mensagem

    ESP_LOGI(CommunicationTAG, "Mensagem enviada com sucesso, msg_id=%d",
             msg_id); // Log de debug

    return msg_id; // Retorna o id da mensagem
}

/**
 * @brief Função responsavel por parar o MQTT
 *
 * @return esp_err_t Resultado ao parar MQTT
 */
esp_err_t stop_mqtt(void) { // Para o MQTT
    if (sys_config->wifi_mode == WIFI_AP_MODE)
        return ESP_FAIL;

    return esp_mqtt_client_stop(global_client); // Para o cliente MQTT
}

/**
 * @brief Função responsavel por configurar o modulo de comunicação
 *
 * @param config ponteiro para otipo de dado abstrato do tipo
 * communication_config com as configurações
 */
void config_communication(communication_config_t *config) {
    sys_config = config;

    ESP_LOGI(CommunicationTAG, "Configurações copiadas com sucesso");
}

/**
 * @brief Função responsavel por obter o endereço IP do WiFi
 *
 * @param ip_address ponteiro para o buffer onde sera salvo o endereço IP
 */
void get_ip_address(unsigned char *ip_address) {      // Retorna o endereço IP
    ESP_LOGI(CommunicationTAG, "IP: %s", ip_address); // Log do endereço IP
}

/**
 * @brief GFunção responsavel por obter o endereço MAC do Dispositivo
 *
 * @param mac_address ponteiro para o buffer onde sera salvo o endereço MAC
 * @return esp_err_t Resultado da operação
 */
esp_err_t get_mac_address(unsigned char *mac_address) {
    esp_err_t ok = ESP_OK;

    if (sys_config->wifi_mode == WIFI_AP_MODE) {
        ok = esp_read_mac(mac_address, ESP_MAC_WIFI_SOFTAP);
    } else {
        ok = esp_read_mac(mac_address, ESP_MAC_WIFI_STA);
    }

    ESP_LOGI(CommunicationTAG, "MAC: %s",
             mac_address); // Log do endereço MAC

    return ok;
}

/**
 * @brief Retorna a configuração
 *
 * @return communication_config_t* ponteiro para configuração
 */
communication_config_t *get_config() { return sys_config; }

#ifdef CONFIG_ENABLE_STORAGE_MANAGER_INTEGRATION
/**
 * @brief Configurar o modulo de comunicação com base nos dados salvos na NVS
 *
 * @param config Ponteiro para um struct de configuração
 */
communication_config_t *config_communication_by_nvs() {
    ESP_LOGI(CommunicationTAG, "Opening Non-Volatile Storage (NVS) handle... ");
    nvs_handle_t my_handle;
    ESP_ERROR_CHECK(nvs_open("storage", NVS_READONLY, &my_handle));

    sys_config =
        (communication_config_t *)malloc(sizeof(communication_config_t));

    nvs_get_i8(my_handle, "auto_ap_init", &sys_config->auto_ap_init);
    nvs_get_i8(my_handle, "wifi_mode", &sys_config->wifi_mode);

    size_t len = 0;

    nvs_get_str(my_handle, "ap_ssid", NULL, &len);
    nvs_get_str(my_handle, "ap_ssid", sys_config->ap_ssid, &len);

    nvs_get_str(my_handle, "ap_password", NULL, &len);
    nvs_get_str(my_handle, "ap_password", sys_config->ap_password, &len);

    nvs_get_str(my_handle, "wifi_ssid", NULL, &len);
    nvs_get_str(my_handle, "wifi_ssid", sys_config->ssid, &len);

    nvs_get_str(my_handle, "wifi_password", NULL, &len);
    nvs_get_str(my_handle, "wifi_password", sys_config->password, &len);

    nvs_get_str(my_handle, "mqtt_server_uri", NULL, &len);
    nvs_get_str(my_handle, "mqtt_server_uri", sys_config->mqtt_server_uri,
                &len);

    nvs_get_str(my_handle, "mqtt_user", NULL, &len);
    nvs_get_str(my_handle, "mqtt_user", sys_config->mqtt_user, &len);

    nvs_get_str(my_handle, "mqtt_password", NULL, &len);
    nvs_get_str(my_handle, "mqtt_password", sys_config->mqtt_password, &len);

    nvs_get_str(my_handle, "mqtt_topic", NULL, &len);
    nvs_get_str(my_handle, "mqtt_topic", sys_config->mqtt_topic, &len);

    nvs_get_str(my_handle, "eap_identify", NULL, &len);
    nvs_get_str(my_handle, "eap_identify", sys_config->eap_identify, &len);

    nvs_get_str(my_handle, "eap_password", NULL, &len);
    nvs_get_str(my_handle, "eap_password", sys_config->eap_password, &len);

    nvs_close(my_handle);

    return sys_config;
}
#endif